#!/usr/bin/env python

class Nba:
  def __init__(self, team, win, loss, home, road):
    self.standing = 1
    self.team = team
    self.win = win
    self.loss = loss
    self.home = home
    self.road = road
    self.w_percentage = self.win/82

  def __repr__(self):
    sep='-'*35
    print(sep)
    out=f'TEAM NAME: {self.team}\nWIN PERCENTAGE: {self.w_percentage:.3f}\nWINS: {self.win}\nLOSSES: {self.loss}\nHOME WINS: {self.home}\nROAD WINS: {self.road}\033[0;0m'
    print(out)
    return sep

  def __gt__(self, other):
    if self.w_percentage > other.w_percentage:
      return True
    else:
      return False  

  def __sub__(self, other):
    out1=f'\n\033[0;33mWIN PERCENTAGE DIFFERENCE ({self.team} and {other.team}):\033[0;0m {abs(self.w_percentage - other.w_percentage):.3f}\n'
    out2= f'\n\033[0;33mWIN DIFFERENCE ({self.team} and {other.team}):\033[0;0m {abs(self.win - other.win)} wins\n'
    out3= f'\n\033[0;33mHOME WIN DIFFERENCE ({self.team} and {other.team}):\033[0;0m {abs(self.home-other.home)} wins at home\n'
    return f'{out1}\n{out2}\n{out3}'

  def __add__(self, other):
    out1=f'\n\033[0;32mWIN PERCENTAGE SUM ({self.team} and {other.team}):\033[0;0m {(self.w_percentage + other.w_percentage):.3f}\n'
    out2=f'\n\033[0;32mWIN SUM ({self.team} and {other.team}):\033[0;0m {self.win + other.win} wins\n'
    out3= f'\n\033[0;32mLOSS SUM ({self.team} and {other.team}):\033[0;0m {self.loss + other.loss} losses\n'
    return f'{out1}\n{out2}\n{out3}'

sep='-='*35
reg='\n\033[1;34mNBA 2018-2019 REGULAR SEASON \033[0;0m\n'
print(sep+reg+sep,end='\n\n')
bos = Nba('BOSTON CELTICS(BOS)', 49, 33, 28, 21)
lal = Nba('LOS ANGELES LAKERS(LAL)', 37, 45, 22, 15)
mil = Nba('MILWAUKEE BUCKS(MIL)', 60, 22, 33, 27)
heat = Nba('MIAMI HEAT(MIA)', 39, 43, 19, 20)
okc = Nba('OKLHOMA CITY THUNDER(OKC)', 46, 36, 27,22)
den = Nba('DENVER NUGGETS(DEN)', 54, 28, 34, 20)
T=[bos,lal,mil,heat,okc,den]
T.sort()
T.reverse()
for i in T:
  for j in T:
    if j>i:
      i.standing += 1

print('\033[1;31m|  STANDINGS  |   TEAM   |   WIN%  |   W  |   L  |   HOME |   ROAD  |')
print('=-'*35+'\033[0;0m')
for k in T:
  print(f"|      {k.standing}      |    {(k.team[k.team.index('(')+1:k.team.index(')')])}   |  {k.w_percentage:.3f}  |  {k.win}  |  {k.loss}  |  {k.home}    |    {k.road}   |\n" )

#Exemplos:
print(okc.__add__(heat))
print(den.__sub__(lal))
print(bos.__repr__())
print(lal.__repr__())
